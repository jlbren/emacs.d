;; bug fix for current emacs version
(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")

;; Keep customization mess out of init.el
(setq custom-file (concat user-emacs-directory "_customize.el"))
(load custom-file t)

;; set up mirrors and paths
(require 'package)
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/"))
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/"))

(setq package-enable-at-startup nil)
(package-initialize)

;; setup use-package to auto update
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; delete trailing whitespace on save
(add-hook 'before-save-hook 'delete-trailing-whitespace)


(eval-when-compile
  (require 'use-package))

;; use bindings in all modes

;; default autocomplete
(use-package auto-complete
  :ensure t)
(ac-config-default)

;; store all backup and autosave files in the system tmp dir
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))
;; disable #. files
(setq create-lockfiles nil)

;; turn off annoying bell
(setq ring-bell-function 'ignore)

;; make backspace delete tabs
(setq backward-delete-char-untabify-method 'hungry)

;; enable autopairs
(electric-pair-mode 1)
;; show matching parens
(show-paren-mode 1)
(setq show-paren-style 'parenthesis)

;; misc packages
(use-package all-the-icons)
(use-package markdown-mode
  :ensure t)
(use-package rainbow-mode
  :ensure t)
(use-package helm
  :ensure t)


;; Bookmarks
(define-key global-map [f9] 'bookmark-set)
(define-key global-map [f11] 'bookmark-jump)
;;define file to use.
(setq bookmark-default-file "~/.emacs.d/bookmarks")
(setq bookmark-save-flag 1)

;; direnv
(use-package direnv
 :config
 (direnv-mode))

(defun load-directory (dir)
  (let ((load-it (lambda (f)
		   (load-file (concat (file-name-as-directory dir) f)))
		 ))
    (mapc load-it (directory-files dir nil "\\.el$"))))

;; load settings
(load-directory "~/.emacs.d/settings")
