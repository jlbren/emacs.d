
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(git-gutter:added-sign "++")
 '(git-gutter:deleted-sign "--")
 '(git-gutter:modified-sign "  ")
 '(lsp-ui-doc-delay 3)
 '(lsp-ui-doc-position (quote top))
 '(lsp-ui-doc-use-webkit t)
 '(lsp-ui-imenu-enable t)
 '(lsp-ui-sideline-enable nil)
 '(package-selected-packages
   (quote
    (yasnippet-classic-snippets company-lsp yasnippet-snippets lsp-python-ms eglot lsp-ui lsp-mode direnv elpy all-the-icons projectile rainbow-mode god-mode all-the-icons-gnus all-the-icons-ivy all-the-icons-gnusjj org-bullets eyebrowse spaceline spacemacs-theme powerline git-gutter magit tmux-pane evil-mode helm evil-mod markdown-mode use-package evil-visual-mark-mode)))
 '(safe-local-variable-values
   (quote
    ((company-backends company-capf)
     (python-shell-interpreter . ".direnv/python-3.5.4/bin/python/Users/jon.brenner/dev/bioinf-archer/.direnv/python-3.5.4/bin/python")
     (eglot-server-programs
      (python-mode "/Users/jon.brenner/dev/bioinf-archer/.direnv/python-3.5.4/bin/pyls"))))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(lsp-ui-doc-background ((t (:background "underPageBackgroundColor")))))
