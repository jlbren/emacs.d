
;; hide gui icons
(menu-bar-mode -1)
(toggle-scroll-bar -1)
(tool-bar-mode -1)

;; disable splashscreen
(setq inhibit-startup-message t)
;; display line number only in code files
(add-hook 'prog-mode-hook #'display-line-numbers-mode)

;; theme
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")
(load-theme `wilmersdorf t)

;; status line
(use-package spaceline
  :demand t
  :init
  (setq powerline-default-separator 'arrow-fade)
  :config
  (require 'spaceline-config)
  (spaceline-spacemacs-theme))
