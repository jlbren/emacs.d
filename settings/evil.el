;; VIM mode
(use-package evil
  :ensure t)

;; enable evil mode by default
(evil-mode t)
;; don't copy deleted code into buffer
(fset 'evil-visual-update-x-selection 'ignore)

;; god mode
(use-package god-mode
  :ensure t)
;; evil gode mode 
(use-package evil-god-state
  :ensure t
  :config
  ;; execute next command in code mode 
  (evil-define-key 'normal global-map
    "," 'evil-execute-in-god-state))

;; enable evil bindings in dashboard
(add-to-list 'evil-emacs-state-modes 'dashboard-mode)

