;; python language server
;; defer loading to make sure direnv
;; has time to load venv path
(use-package lsp-python-ms
  :ensure t
  :hook (python-mode . (lambda ()
                          (require 'lsp-python-ms)
                          (lsp-deferred))))


;; use company for completion backend
(use-package company-lsp
  :ensure t)
(push 'company-lsp company-backends)
