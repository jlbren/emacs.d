(use-package page-break-lines
  :ensure t)
(turn-on-page-break-lines-mode)

;; splashscreen dashboard
(use-package dashboard
  :ensure t
  :config
  (dashboard-setup-startup-hook)
  (setq dashboard-startup-banner 1)
  (setq dashboard-items '((recents  . 5)
                        (projects . 5)
                        (bookmarks . 5))))
                        ;; (agenda . 5)
                        ;; (registers . 5))))

;; set scratch buffer to dashboard
(setq initial-buffer-choice (lambda () (get-buffer "*dashboard*")))
