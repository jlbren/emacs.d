;; org mode settings
(require 'org)
(setq org-hide-emphasis-markers t)
(setq org-indent-mode t)
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)

;; replace headlines with bullets
(use-package org-bullets
  :config
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

(use-package eyebrowse
  :diminish eyebrowse-mode
  :config (progn
	    (define-key eyebrowse-mode-map (kbd "M-1")
	      'eyebrowse-switch-to-window-config-1)
	    (define-key eyebrowse-mode-map (kbd "M-2")
	      'eyebrowse-switch-to-window-config-2)
            (define-key eyebrowse-mode-map (kbd "M-3")
	      'eyebrowse-switch-to-window-config-3)
            (define-key eyebrowse-mode-map (kbd "M-4") '
	      eyebrowse-switch-to-window-config-4)
            (define-key eyebrowse-mode-map (kbd "C-c C-w C-x") '
	      eyebrowse-close-window-config)
	    (eyebrowse-mode t)
	    (setq eyebrowse-new-workspace "*dashboard*")))

;; pretify org mode symbols
(add-hook 'org-mode-hook (lambda ()
   "Beautify Org Checkbox Symbol"
   (push '("[ ]" .  "☐") prettify-symbols-alist)
   (push '("[X]" . "☑" ) prettify-symbols-alist)
   (push '("[-]" . "❍" ) prettify-symbols-alist)
   (prettify-symbols-mode)))
