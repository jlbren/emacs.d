(require 'bind-key)
;; open buffer list in current window
(bind-key* "C-c C-b" 'buffer-menu)

;; windows bindings
(bind-key* "C-c v" 'split-focused-window-below)
(bind-key* "C-c h" 'split-focused-window-right)
(bind-key* "M-RET" 'other-window)
(bind-key* "C-c x" 'delete-window)
(bind-key* "C-c C-x" 'delete-other-windows)
(bind-key* "C-c C-k" 'windmove-up)
(bind-key* "C-c C-j" 'windmove-down)
(bind-key* "C-c C-h" 'windmove-left)
(bind-key* "C-c C-l" 'windmove-right)
(bind-key* "C-c C-c" 'make-frame-command)

(defun split-focused-window-below ()
  "Open a focused window below current location"
  (interactive)
  (split-window-below)
  (windmove-down 1))

(defun split-focused-window-right ()
  "Open a focused window to the right of current location"
  (interactive)
  (split-window-right)
  (windmove-right 1))


;; winner mode (restore screen layouts)
(when (fboundp 'winner-mode)
  (winner-mode 1))
