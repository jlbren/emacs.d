
;; projectile (git projects)
;; set project roots
(use-package projectile
  :ensure t
  :config
  ;;(define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
  (define-key projectile-mode-map (kbd "C-c C-p") 'projectile-command-map)
  (projectile-mode +1)
  (setq projectile-project-search-path '("~/dev/" "~/repos/")))
