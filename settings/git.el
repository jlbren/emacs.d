;; git-fugitive replacement TODO learn
 (use-package magit
   :ensure t)
 (use-package git-gutter
   :ensure t
   :config 
     (custom-set-variables
      '(git-gutter:modified-sign "  ") ;; two space
      '(git-gutter:added-sign "++")    ;; multiple character is OK
      '(git-gutter:deleted-sign "--"))

     (set-face-background 'git-gutter:modified "#a6c1e0") 
     (set-face-foreground 'git-gutter:added "#7ebebd")
     (set-face-foreground 'git-gutter:deleted "#e1c1ee"))

;; enable git gutter on all files
(global-git-gutter-mode +1)
